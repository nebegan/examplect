﻿using NUnit.Framework;

namespace CozyTimeClientTest
{
    [TestFixture]
    public class Tests
    {
        private readonly CozyTimeClient _client = new CozyTimeClient(null);

        [Test]
        public void Authorize()
        {
            _client.Authorize("375259005003");
        }

        [Test]
        public void SubmitCode()
        {
            _client.SubmitCode("375259005003", "559793");
        }
    }
}