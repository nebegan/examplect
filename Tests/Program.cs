﻿using System;
using System.Collections.Generic;
using System.Threading;
using CozyTime.Client;
using QuobjectClinet;

namespace CozyTimeClientTest
{
    class Program
    {
        static void Main(string[] args)
        {
            SocketsTest();
        }

        //private static void JoinGameTest()
        //{
        //    for (int i = 0; i < 10; i++)
        //    {
        //        var client = new CozyTimeClient(null);
        //        client.Authorize("37525900501" + i);
        //    }
        //}

        //private static void FullGameTest()
        //{
        //    var clients = Get5Clients().ToList();
        //    var host = clients[0].Value;
        //    var players = clients.Skip(1).ToList();
        //    var gameId = host.CreateGame(players.Select(x => x.Key), 60);
        //    foreach (var player in players)
        //    {
        //        player.Value.JoinGame(gameId);
        //    }
        //    host.StartGame(gameId);
        //    host.StopGame(gameId);
        //}

        //private static void ApproveTest()
        //{
        //    Approve("375259005011", "954565");
        //    Approve("375259005012", "122876");
        //    Approve("375259005013", "240664");
        //    Approve("375259005014", "203946");
        //    Approve("375259005015", "411800");
        //}

        //private static void Approve(string phone, string code)
        //{
        //    var client = new CozyTimeClient(null);
        //    client.SubmitCode(phone, code);
        //}

        //private static void FriendsTest()
        //{
        //    var client = Get5Clients().First().Value;
        //    var result = client.GetFriends(new List<string>() { "375259005011" , "375259005012" , "375259005013" , "375259005014", "375259005015"});
        //}

        private static void BasicTest()
        {
            var client = new CozyTimeClient(null, "http://128.199.49.60/");
            client.Authorize("375259005003");
            client.SubmitCode("375259005003", "499379");
            client.SetDisplayName("Alex Shkor");
            client.CreateGame(new List<string> { "1", "2", "3" }, 60);
        }

        //private static IEnumerable<KeyValuePair<string, CozyTimeClient>> Get5Clients()
        //{
        //    yield return new KeyValuePair<string, CozyTimeClient>("44942718f1f88e1661ebd6532c3698e6", new CozyTimeClient("b01bf11643c99e93f8445471cda67be4ef8ca3a1d1a5dec7f30deb25bf146ee95fe470e9b919a18a7c43632e5b3c971853c36b03c530ca14f9db1844a1bf9bcbb5f7a5eed2fbadbd2ec292609f0b22b96a2d543135973c79d7d7d3115a88a97cc109e8c5860c9708358760b2475822d062b90016dd8010ce22464267b643c29d949ca8644e824958449d4eb51d13f0e5c9f1ceab1a3a60a55dfe982645ebd173afb0614c7d25623053c8f7605a08b0acddf84f33988a721a82f5a5e615e2977cb9b5248c0734a12c5badd26a077e09e9e0398642cb7aec82234438d4e5242ad95a4d31949a0330a144b63dff1ae8b283299518ed8a9cbe3efc4d83a45cbf6334"));
        //    yield return new KeyValuePair<string, CozyTimeClient>("31fca30abce3a7f6cae65178540dbcde", new CozyTimeClient("907a1b65df802673f336bd6e3605b5594bcaef7f59a52b28c8b23f8cc2462efc9cf9dc974c4c7c25eb9d6971d3be0cf0ef84277da1dbbd3af38b18c400eda3668a106467e16d6ba15fbe1a65ce635c9152e2c28d19ba1317da1fb2a2524a2c9082a035a2e241766cf61c35ba1dda61830d3edffdf8b177522f95cabfa08c3facdb58b677265c82d7f2e6734162df39e9d9e2f0bc7472e1c507a2582f3c56f4c67ce7d4876b8dde3270065db4c52a3d5e941ef4ebade1684094e9bf2f44c54dcbbb4b8c01fce5a1816dcb6e3c68e692085ed9f28735bed68f478981b1f5c6ee13ee860546b2707fa49206a35369e574333a5979dcb67408d50fc6f1f83b64db20"));
        //    yield return new KeyValuePair<string, CozyTimeClient>("325fcbbd82d80f2cb1f77d3c038cd0eb", new CozyTimeClient("09a65315b36868a1f6c6d80001d5cb7b9028af8f3fbbb24112e186f67f8155f4e5947d512d9668250f8dce237fb90a896a8128c77c090b4621d486625f52c3a372d4d7a02ccc2140f5871910774fea37d9142fa337810dedeffe76b0ba412db70af4142f3b0c9251fdaaec02ea98fe09d882cd06a7dcb15738b2b02b571c2de8dde416d4dc29659fd624d7f630fa0a7df1862a50fc747ef73f4d7c2f7af7c8691062a44cb46edfa9ced38a9188c61a6133510b2fdb94f753c93c0d25ceb405bfdbee0011dde69ce1f5baeeaa551b34067713b4cfa9a7b1270b2430f22b9f174498a945d166d4eed2e71e8ce932e0fc3827ca4812a09496a19b630f726d345e3a"));
        //    yield return new KeyValuePair<string, CozyTimeClient>("a2f6d2c55070ae71cb9d9d7ef01695e6", new CozyTimeClient("a4ca5a49796be595eb72668c9708b77f0c9727003a1c7c55e3b81fbf3fbd5d333cc1c2ebe7f9de46be2410cd8d45b8391793cc1e0a99dec810e935bec97a8ab789beb6843ca6bb63f464ca48998cdf88758a9fe7239b3f9a668074c849c32b18842b50d05ae469b500a9e79d1407e6848e041b7fc8008c15ff195050c428b9f66d7f396f5a3b7e4df02d44aba486f6602774436f652dc56714e25d4c2f2c6aa230b0f810665b9419d8b3754054c6b487927f4b6ee8996972a5439d2f22f71fb006224bb6b5247e4e67b5fd96a438e966a8e728a9345e3ac5af729c73ac8f72590690a3e55c2ffc6d650bfd436fc3c476aad8e6a81a24f5b06f0bb7f369c945d9"));
        //    yield return new KeyValuePair<string, CozyTimeClient>("15fc59caaf121f55ddffe5231f955ef9", new CozyTimeClient("711a4219df8ae423df8dd2e1ccac5989b784cbde141328d40a69f8273675b44063025c78579a71d4088d1d1271ee1491391b1126f2cf9be5361b0e7451b66d2122df0a4594725bc23752a098ca1cad037deb0867795ffa5a51a8a961cfad59223728bbdf6a56c28b246318f96bf85a79326ce8bf882157a93f3f80be098759ca03d1aef40fc266764a216a6c5f92d40eb8b768db857972b85ea45552fd3f202705a550780b05152ecce0b6691e580f075ff3bbda5ee46f583028f17df4694ca40fd67f20e0cc094b24876adaa0c94e8ee598701b4b1fc9d24281995b21064f5fa8c27eced68c9040b10f1fe69a133ee4e4c562307ea6c225f139f3719e798eb1"));
        //} 

        private static void SocketsTest()
        {
            try
            {
                
            var sockets = new SignalsObserver(new QuobjectClient("a9748fabaac3637b0ef8e342fd8987ad7dac5d64fe590b26fb7f835b4da286b14d81d0b1e50b0e5636a948f5acd63a90fba22d8af1fc8120ccc140b6f29c35ec76fe14a260724b1a5c695331b07231f152b846c52d895c9e8fc3b34f3f299308735e2b4ec3fc3f2e3ab99db944811c487941fb21010018369720fb8b9e96f9061691265d5ba61a8a251a133c1f2443f919bd671852c34415e9a9a274d6c2e1af3f827051b71040606a00c8b25a553e835077f9ca72869c200005735643f61fda66e02945b32a458ca5b83150e6e90b4574a9278cae4c723ed8a5c5be07c5bfad0dcb80808f39f4339cfcde7cc969ed3ec361375848743a093860f61afbb0c98b"));
            sockets.Invited += (sender, dto) =>
            {
                Console.WriteLine("Invited!");
            };
           Thread.Sleep(10000);
            var client = new CozyTimeClient("85239a715559eb1b11a84119606882493f11c89ebaa7f8a06658839fe48e9a158d149a72e360fbce58b218df198b1ffa1814ea7e2c6a21a98687b18585e99fdf2408c50ef27e32fea2fab39a4c433bde68519e20b29874f6f0b1184eb1ceebc6ec88b9cbd934eb71b792fcf4bd1c8669c353ffdb01cc9e9cfa22121a95248e5b351934e181869e8c3f1a8caa66813a255b385e662b222970d482aa90e3bc18b0058a6b20fb846c3b2ab9e9dbc88e6eb79c0ce854f4aaa620b76a7edbe28c3d5dc1d9cad60d144bd08a73b0999bbefb311018529fb8039f75c6f733c9f205ab90c60ee4c7262b43ca9f80cb03949bfaa3230490ebce736212d9a2e995c6d55159",
                "http://128.199.49.60/");
            client.CreateGame(new List<string>() {"aa723bfeef0b9b565bcf9ea6e88020b1"}, 15);
                Console.ReadKey();
                
            }
            catch (Exception e)
            {

                throw;
            }
        }
    }
}
