﻿using System.Collections.Generic;
using System.Linq;
using CozyTime.Client.Models;
using System;

namespace CozyTime.Client
{
    public class ManualSignalsObserver
    {
        private readonly CozyTimeClient _client;

        public event Invited Invited;

        public ManualSignalsObserver(CozyTimeClient client)
        {
            _client = client;
        }

        private Dictionary<string, GameDto> _invites = null;

        public void UpdateInvited(DateTime? newFrom = null)
        {
            var newInvites = _client.GetMyGames();
            GameDto inviteToFire = null;
            if (_invites == null)
            {
                if (newFrom == null)
                {
                    _invites = newInvites.ToDictionary(k => k.Id, dto => dto);
                }
                else
                {
                    _invites = newInvites.Where(x => x.Created <= newFrom).ToDictionary(k => k.Id, dto => dto);
                    newInvites = newInvites.Where(x => x.Created > newFrom).ToList();
                }
            }
            else
            {
                foreach (var newInvite in newInvites)
                {
                    if (!_invites.ContainsKey(newInvite.Id))
                    {
                        _invites.Add(newInvite.Id, newInvite);
                        if (inviteToFire == null || inviteToFire.Created < newInvite.Created)
                        {
                            inviteToFire = newInvite;
                        }
                    }
                }
            }
            if (inviteToFire != null)
            {
                Invited.Invoke(this, inviteToFire);
            }
        }
    }
}