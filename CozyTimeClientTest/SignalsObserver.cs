﻿using System;
using CozyTime.Client.Models;
using Newtonsoft.Json;

namespace CozyTime.Client
{
    public class SignalsObserver
    {
        public event Invited Invited;
        public event Joined Joined;
        public event Left Left;
        public event GameFinished GameFinished;
        public event GameStarted GameStarted;

        private class Events
        {
            public const string Invited = "user-invited";
            public const string Joined = "user-joined";
            public const string Left = "user-left";
            public const string GameFinished = "game-finished";
            public const string GameStarted = "game-started";
        }

        public SignalsObserver(ISocketsClient client)
        {
            client.On(Events.Invited, (data) =>
            {
                var dto = JsonConvert.DeserializeObject<GameDto>((string) data);
                Invited.Invoke(this, dto);
            });
            client.On(Events.Joined, (data) =>
            {
                var dto = JsonConvert.DeserializeObject<UserDto>((string) data);
                Joined.Invoke(this, dto);
            });
            client.On(Events.Left, (data) =>
            {
                var dto = JsonConvert.DeserializeObject<UserDto>((string) data);
                Left.Invoke(this, dto);
            });
            client.On(Events.GameStarted, (data) =>
            {
                var dto = JsonConvert.DeserializeObject<GameDto>((string) data);
                GameStarted.Invoke(this, dto);
            });
            client.On(Events.GameFinished, (data) =>
            {
                var dto = JsonConvert.DeserializeObject<GameDto>((string) data);
                GameFinished.Invoke(this, dto);
            });
            client.Connect();
        }
    }

    public interface ISocketsClient
    {
        void On(string e, Action<string> on);
        void Connect();
    }

    public delegate void GameStarted(object sender, GameDto gameDto);

    public delegate void GameFinished(object sender, GameDto gameDto);

    public delegate void Left(object sender, UserDto args);

    public delegate void Joined(object sender, UserDto user);

    public delegate void Invited(object sender, GameDto gameDto);

    public delegate void NoArguments(object sender);
}