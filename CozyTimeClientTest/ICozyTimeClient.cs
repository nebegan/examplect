using System.Collections.Generic;
using CozyTime.Client.Models;

namespace CozyTime.Client
{
    public interface ICozyTimeClient
    {
        void Authorize(string phone);
        void SubmitCode(string phone, string code);
        void SetDisplayName(string name);
        List<UserDto> GetFriends(List<string> phoneNumbers);
        GameDto JoinGame(string gameId);
        StopGameReponse StopGame(string gameId);
        StartGameReponse StartGame(string gameId);
        StartGameReponse LeaveGame(string gameId);
        List<GameDto> GetMyGames();
        GameDto GetGame(string gameId);
        StartGameReponse DeleteGame(string gameId);
        string CreateGame(List<string> players, int targetTime);
        void Log(LogLevel level, string message);
    }

    public enum LogLevel
    {
        Info,
        Warning,
        Error,
        Critical
    }
}