﻿using System;
using System.Collections.Generic;

namespace CozyTime.Client.Models
{
    public class GameDto
    {
        private List<UserDto> _users;
        private bool _joinedUpdated;

        public string Id { get; set; }

        public List<string> Invited { get; set; }
        
        public List<string> Joined { get; set; }
        
        public int TargetTime { get; set; }

        public string Owner { get; set; }

        public bool IsStarted { get; set; }

        public bool IsStopped{ get; set; }

        public bool IsDeleted { get; set; }

        public DateTime Created { get; set; }

        public List<UserDto> Users
        {
            get
            {
                if (!_joinedUpdated)
                {
                    foreach (var userDto in _users)
                    {
                        userDto.Joined = Joined.Contains(userDto.Id);
                    }
                    _joinedUpdated = true;
                }
                return _users;
            }
            set { _users = value; }
        }
    }
}