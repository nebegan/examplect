﻿using System.Collections.Generic;

namespace CozyTime.Client.Models
{
    public class GetFriendsResponse
    {
        public List<UserDto> Friends { get; set; } 
    }
}