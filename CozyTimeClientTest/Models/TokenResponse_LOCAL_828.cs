﻿using System;

namespace CozyTimeClientTest
{
    public class TokenResponse
    {
        public string Id { get; set; }
        public string PhoneNuber { get; set; }
        public string Code { get; set; }
        public string Token { get; set; }
        public string Name { get; set; }
    }
}