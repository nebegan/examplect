﻿namespace CozyTime.Client.Models
{
    public class AuthorizeRequest
    {
        public string PhoneNumber { get; set; }
        public string Code { get; set; }
    }
}