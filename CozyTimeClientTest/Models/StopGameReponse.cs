namespace CozyTime.Client.Models
{
    public class StopGameReponse
    {
        public string GameId { get; set; }
    }
}