﻿namespace CozyTime.Client.Models
{
    public class StartGameRequest
    {
        public string GameId { get; set; }
    }
}