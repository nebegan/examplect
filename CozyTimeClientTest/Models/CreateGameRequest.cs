﻿using System.Collections.Generic;

namespace CozyTime.Client.Models
{
    public class CreateGameRequest
    {
        public List<string> Players { get; set; }
        public int TargetTime
        { get; set; }
    }
}