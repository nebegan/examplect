﻿using System.Collections.Generic;

namespace CozyTime.Client.Models
{
    public class GetFriendsRequest
    {
        public List<string> PhoneNumbers { get; set; }
    }
}