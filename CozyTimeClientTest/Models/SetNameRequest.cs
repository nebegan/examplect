namespace CozyTime.Client.Models
{
    public class SetNameRequest
    {
        public string Name { get; set; }
    }
}