﻿using System;

namespace CozyTime.Client.Models
{
    public class LogRequest
    {
        public string Message { get; set; }

        public string Level { get; set; }

        public DateTime ClientDatTime { get; set; }
    }
}