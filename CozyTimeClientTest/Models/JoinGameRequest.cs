﻿namespace CozyTime.Client.Models
{
    public class JoinGameRequest
    {
        public string GameId { get; set; }
    }
}