﻿namespace CozyTime.Client.Models
{
    public class StartGameReponse
    {
        public string GameId { get; set; }
    }
}