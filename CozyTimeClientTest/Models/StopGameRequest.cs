﻿namespace CozyTime.Client.Models
{
    public class StopGameRequest
    {
        public string GameId { get; set; }
    }
}