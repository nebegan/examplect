﻿using System.Security.Policy;

namespace CozyTime.Client.Models
{
    public class UserDto
    {
        public string Id { get; set; }
        public string DisplayName { get; set; }
        public string PhoneNumber { get; set; }
        public  bool Joined { get; set; }
    }
}