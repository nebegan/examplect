﻿using System;
using System.Collections.Generic;
using System.Net;
using CozyTime.Client.Models;
using Newtonsoft.Json;
using RestSharp;

namespace CozyTime.Client
{
    public class CozyTimeClient: ICozyTimeClient
    {
        private const string TokenHeader = "Authorization";
        private const string TokenPrefix = "Bearer ";

        private string _bearerToken;
        private readonly string _baseUrl = "http://192.168.10.132:3001/"; //local ubuntu VM on Alex Shkor work machine

        public IRestResponse LastResponse { get; private set; }

        public CozyTimeClient(string bearerToken = null, string baseUrl = null)
        {
            _bearerToken = bearerToken;
            if (baseUrl != null)
            {
                _baseUrl = baseUrl;
            }
        }

        public string BearerToken
        {
            get { return _bearerToken; }
        }

        public void Authorize(string phone)
        {
            Execute<TokenResponse>("authorize", new AuthorizeRequest{ PhoneNumber = phone });
        }

        public void SubmitCode(string phone, string code)
        {
            var response = Execute<TokenResponse>("submitcode", new AuthorizeRequest { PhoneNumber = phone, Code = code});
            if (response != null && response.Token != null)
            {
                _bearerToken = response.Token;
            }
        }

        public void SetDisplayName(string name)
        {
            Execute<Dictionary<string, string>>("api/setname", new SetNameRequest { Name = name});
        }

        public StartGameReponse DeleteGame(string gameId)
        {
            return Execute<StartGameReponse>("api/game/delete", new StartGameRequest { GameId = gameId });
        }

        public string CreateGame(List<string> players, int targetTime)
        {
            return Execute<CreateGameReponse>("api/game/create", new CreateGameRequest { Players = players, TargetTime = targetTime}).GameId;
        }

        public void Log(LogLevel level, string message)
        {
            Execute<LogRequest>("log",
                new LogRequest {ClientDatTime = DateTime.Now, Level = level.ToString(), Message = message});
        }

        public StartGameReponse StartGame(string gameId)
        {
            return Execute<StartGameReponse>("api/game/start", new StartGameRequest { GameId = gameId });
        }

        public StartGameReponse LeaveGame(string gameId)
        {
            return Execute<StartGameReponse>("api/game/leave", new StartGameRequest { GameId = gameId });
        }

        public List<GameDto> GetMyGames()
        {
            return Execute<List<GameDto>>("api/game/all", new object());
        }

        public GameDto GetGame(string gameId)
        {
            return Execute<GameDto>("api/game/get", new StartGameRequest { GameId = gameId });
        }

        public StopGameReponse StopGame(string gameId)
        {
            return Execute<StopGameReponse>("api/game/stop", new StopGameRequest { GameId = gameId });
        }

        public GameDto JoinGame(string gameId)
        {
            return Execute<GameDto>("api/game/join", new JoinGameRequest{ GameId = gameId });
        }

        public List<UserDto> GetFriends(List<string> phoneNumbers)
        {
            return Execute<List<UserDto>>("api/findbyphones", new GetFriendsRequest { PhoneNumbers = phoneNumbers });
        }

        private T Execute<T>(string path, object body) where T : new()
        {
            var client = new RestClient(_baseUrl);
            var request = new RestRequest(path, Method.POST);
            if (_bearerToken != null)
            {
                request.AddHeader(TokenHeader, TokenPrefix + _bearerToken);
            }
            var json = JsonConvert.SerializeObject(body);
            request.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);
            request.RequestFormat = DataFormat.Json;
            var response = client.Execute(request);
            LastResponse = response;
            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new InvalidOperationException(response.ErrorMessage + "\n url: " + response.ResponseUri + "\n json: " + json);
            }
            try
            {
                return JsonConvert.DeserializeObject<T>((response.Content ?? "{}").Trim());
            }
            catch
            {
                return default(T);
            }
        }
    }
}