﻿using System.Linq;
using CozyTime.Client.Models;

namespace CozyTime.Client
{
    public class GameObserver
    {
        private readonly GameDto _game;
        private readonly CozyTimeClient _client;

        public event Joined Joined;
        public event Left Left;
        public event GameFinished GameFinished;
        public event GameStarted GameStarted;

        public GameObserver(GameDto game, CozyTimeClient client)
        {
            _game = game;
            _client = client;
        }

        public void Update()
        {
            var dto = _client.GetGame(_game.Id);
            var joined = dto.Joined.Except(_game.Joined).ToList();
            foreach (var id in joined)
            {
                    _game.Joined.Add(id);
                    var user = dto.Users.Find(x => x.Id == id);
                    Joined(this, user);
            }
            var left = _game.Joined.Except(dto.Joined).ToList();
            foreach (var id in left)
            {
                _game.Joined.Remove(id);
                var user = dto.Users.Find(x => x.Id == id);
                Left(this, user);
            }
            if (!_game.IsStarted && dto.IsStarted)
            {
                _game.IsStarted = true;
                GameStarted(this, _game);
            }
        }
    }
}