﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SocketIO;

namespace SocketsTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var socket = new SocketIOComponent();
            socket.url = "ws://128.199.49.60/socket.io/";
            socket.Awake();
            socket.Connect();
        }
    }
}
