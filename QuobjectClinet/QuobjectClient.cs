﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CozyTime.Client;
using Quobject.SocketIoClientDotNet.Client;

namespace QuobjectClinet
{
    public class QuobjectClient: ISocketsClient
    {
        private readonly string _token;
        private Socket _socket;

        public QuobjectClient(string token)
        {
            _token = token;
          _socket = IO.Socket("ws://128.199.49.60/socket.io/");
           
        }

        public void On(string e, Action<string> @on)
        {
            _socket.On(e, o => on(o.ToString()));
        }

        public void Connect()
        {
            _socket.On(Socket.EVENT_CONNECT, () =>
            {
                _socket.Emit("userconnect", _token);
            });
            _socket.On(Socket.EVENT_ERROR, (err) =>
            {
                Console.WriteLine(err);

            });
            _socket.Connect();
        }
    }
}
