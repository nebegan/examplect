﻿using System;
using System.Collections.Generic;
using CozyTime.Client;

namespace DefaultClientTest
{
    public class DefaultSocketsClient : ISocketsClient
    {
        private readonly string _token;
        private readonly SocketIOClient.Client _client;
        private readonly Dictionary<string, Action<object>> _subscriptions;

        public DefaultSocketsClient(string token)
        {
            _token = token;
            _subscriptions = new Dictionary<string, Action<object>>();
            _client = new SocketIOClient.Client("ws://128.199.49.60/socket.io/");
        }

        public void Connect()
        {
            _client.Connect();

            _client.Emit("userconnect", _token);
        }

        public void On(string e, Action<string> on)
        {
            _subscriptions[e] = (data) => on((string)data);
        }
    }
}
